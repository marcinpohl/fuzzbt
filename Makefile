#!/usr/bin/make -f

MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-variables
MAKEFLAGS += --no-builtin-rules

SHELL := /bin/bash
.SHELLFLAGS := -u -o pipefail -c

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
ROOT_DIR    := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MKFILE_PATH))))
### GNU make also has 'CURDIR' variable built in
VERSION_GIT := $(shell git describe --tag --always --abbrev=5 --dirty)


### Major application-specific variables you want to use. hopefully names are self-explanatory:
DOCKER_OPTS ?= --init --security-opt=no-new-privileges --cap-drop ALL
### probably want CAP_SYS_ADMIN for linux-perf
### Generics done above


BASE_NAME	:= fuzzbt
STAGE0_IMG  := $(BASE_NAME).stage0
STAGE1_IMG  := $(BASE_NAME).stage1
STAGE0_FILE := $(ROOT_DIR)/flags/$(STAGE0_IMG)
STAGE1_FILE := $(ROOT_DIR)/flags/$(STAGE1_IMG)

BLAH        := 0 1
STAGES		= $(addprefix $(BASE_NAME).stage, $(BLAH))
IMG2FILE    = $(addprefix $(ROOT_DIR)/flags/, $(STAGES))


img:
	@echo $(STAGES)
	@echo $(IMG2FILE)

test:
	@echo BASE_NAME
	@echo $(BASE_NAME)
	@echo
	@echo STAGE*_IMG
	@echo $(STAGE0_IMG)
	@echo $(STAGE1_IMG)
	@echo
	@echo STAGE*.FILE
	@echo $(STAGE0_FILE)
	@echo $(STAGE1_FILE)

run: $(STAGE1_FILE)
	docker run $(DOCKER_OPTS) -it --rm $(STAGE1_IMG)
debug: $(STAGE0_FILE)
	docker run $(DOCKER_OPTS) -it --rm $(STAGE0_IMG)


### :FIXIT:  the general pattern should be transforming a Dockerfile/target into a docker inspect dump.  not there yet
stage0: $(STAGE0_FILE)
$(STAGE0_FILE):
	-docker pull python:3.8-slim-buster
	docker build \
		--file Dockerfile.pythonslimbuster \
		--tag $(STAGE0_IMG) \
		--target=stage0 \
		.
	docker image inspect $(STAGE0_IMG) > $@

### For this example it's largely redundant, stage0 runs bash, stage1 runs ipython
stage1: $(STAGE1_FILE)
$(STAGE1_FILE): $(STAGE0_FILE)
	docker build \
		--file Dockerfile.pythonslimbuster \
		--tag $(STAGE1_IMG) \
		--target=stage1 \
		--cache-from=$(STAGE0_IMG) \
		.
	docker image inspect $(STAGE1_IMG) > $@

clean:
	-docker image rm $(STAGE0_IMG)
	-rm $(STAGE0_FILE)
	-docker image rm $(STAGE1_IMG)
	-rm $(STAGE1_FILE)

.PHONY: stage0 $(STAGE1_FILE)