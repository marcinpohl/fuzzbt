#!/usr/bin/env python3

def use_mutable_default_param(idx=0, ids=[]):
    ids.append(idx)
    print(idx)
    print(ids)
# use_mutable_default_param(idx=1)
# use_mutable_default_param(idx=2)


def broad_exceptions(a,b):
    try:
        c = a / b
    except:
        ### Too noisy
        # print('oops something went bad but i wont tell you what')
        ### So instead lets make it double awful, and squash the broad exceptions
        pass

# broad_exceptions(1,-0.0)

def simple_open(path):
    fd = open(path)
    data = fd.readlines()
    return data


def transcode_file(request, filename):
    import subprocess
    command = 'ffmpeg -i "{source}" output_file.mpg'.format(source=filename)
    subprocess.call(command, shell=True)  # a bad idea!


if __name__ == '__main__':
    from battle_tested.beta import fuzz
    ### very noisy does not want to terminate in sane time
    #fuzz(transcode_file)

    fuzz(simple_open)

    fuzz(broad_exceptions)

    fuzz(use_mutable_default_param)